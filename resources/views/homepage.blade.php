<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laravel</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/bootstrap-theme.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/swiper.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/homepage.css')}}" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top navbar-fixed-top-custom">
      <button class="c-hamburger c-hamburger--htla" id="menu-button">
        <span>toggle menu</span>
      </button>
    </nav>
    <ul id="menu">
      <li><a title="News" href="/news">News</a></li>
      <li><a title="Video" href="/video">Video</a></li>
      <li><a title="Menu 3" href="#">Menu 3</a></li>
      <li><a title="Menu 4" href="#">Menu 4</a></li>
      <li><a title="Menu 5" href="#">Menu 5</a></li>
      <li><a title="Menu 5" href="#">Menu 5</a></li>
      <li><a title="Menu 6" href="#">Menu 6</a></li>
      <li><a title="Menu 7" href="#">Menu 7</a></li>
      <li><a title="Menu 8" href="#">Menu 8</a></li>
      <li><a title="Menu 9" href="#">Menu 9</a></li>
      <li><a title="Menu 10" href="#">Menu 10</a></li>
    </ul>
    <div class="container">
        <div class="container-header">
          <object type="image/svg+xml" data="{{asset('images/icons/film-strip.svg')}}">Your browser does not support SVG</object>
          <p>Interested Video</p>
        </div>
        <div id="video-container">
          <ul id="hot-video-nav">
            <li id="movie-menu" class="current"><object type="image/svg+xml" data="{{asset('images/icons/clapperboard.svg')}}">Your browser does not support SVG</object></li>
            <li id="music-menu"><object type="image/svg+xml" data="{{asset('images/icons/quaver.svg')}}">Your browser does not support SVG</object></li>
            <li id="psy-menu"><object type="image/svg+xml" data="{{asset('images/icons/brain-in-head.svg')}}">Your browser does not support SVG</object></li>
            <li id="funny-menu"><object type="image/svg+xml" data="{{asset('images/icons/mask.svg')}}">Your browser does not support SVG</object></li>
          </ul>
          <div class="hot-video" id="movie">
            <div class="video-title"><p class="text-center">WARCRAFT Movie Trailer (2016)</p></div>
            <div class="embed-responsive embed-responsive-16by9 video-content">
              <iframe class="embed-responsive-item"  width="560" height="315" src="https://www.youtube.com/embed/RhFMIRuHAL4?showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
          <div class="hot-video hide" id="music">
            <div class="video-title"><p class="text-center">Katy Perry - Roar (Official)</p></div>
            <div class="embed-responsive embed-responsive-16by9 video-content">
              <iframe class="embed-responsive-item"  width="560" height="315" src="https://www.youtube.com/embed/CevxZvSJLk8?showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
          <div class="hot-video hide" id="psychology">
            <div class="video-title"><p class="text-center">Intro to Psychology - Crash Course Psychology #1</p></div>
            <div class="embed-responsive embed-responsive-16by9 video-content">
              <iframe class="embed-responsive-item"  width="560" height="315" src="https://www.youtube.com/embed/vo4pMVb0R6M?showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
          <div class="hot-video hide" id="funny">
            <div class="video-title"><p class="text-center">Funny Kids Fails 2016 || A Fail Compilation by FailArmy</p></div>
            <div class="embed-responsive embed-responsive-16by9 video-content">
              <iframe class="embed-responsive-item"  width="560" height="315" src="https://www.youtube.com/embed/5Qoq-43o_MY?showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      <div class="container-header">
        <object type="image/svg+xml" data="{{asset('images/icons/newspaper-report.svg')}}">Your browser does not support SVG</object>
        <p>Lastest News</p>
      </div>
      <div class="swiper-container" id="news-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide news">
              <img src="http://static.thanhnien.com.vn/Uploaded/ngocthanh/2016_03_05/bttuhnhth_WBHO.jpg" class="img-responsive pull-left news-image" />
              <div class="news-title">Đặt mình vào vị trí doanh nghiệp xem có chịu được không? </div>
            </div>
            <div class="swiper-slide news">
              <img src="http://imgs.vietnamnet.vn/Images/2016/03/04/16/20160304164342-hoa-8-3.jpeg" class="img-responsive pull-left news-image"/>
              <div class="news-title">Bó hồng Nhật 60 triệu đại gia tặng bạn gái 8/3 </div>
            </div>
            <div class="swiper-slide news">
              <img src="http://img.f33.dulich.vnecdn.net/2016/02/29/cavoi-1456729724_180x108.jpg" class="img-responsive pull-left news-image"/>
              <div class="news-title">Những hòn đảo mang tên loài vật ở Việt Nam</div>
            </div>

        </div>
        <div class="swiper-pagination"></div>
      </div>

      <footer>
        <p class="text-center">Copyright 2016 by Phu Le</p>
      </footer>
    </div>

    <script src="{{asset('js/vendor/jquery.min.js')}}"></script>
    <script src="{{asset('js/vendor/swiper.jquery.min.js')}}"></script>
    <script src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/homepage.js')}}"></script>
  </body>
</html>
