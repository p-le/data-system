@extends('layouts.default')

@section('css')
  <link href=" {{asset('css/radio.css')}} " rel="stylesheet" type="text/css"/>
@stop

@section('content')
@if(isset($radio))
<div class="container">
  <div class="player-container">
    <div class="cover">
        <img src=" {{ $radioSong->cover_url }}" class="img-responsive"/>
    </div>
    <div class="player">
      <p class="radio-title">{{ $radio->name }}</p>
      <h4 class="song-info">{{ $radioSong->name }} - {{ $radioSong->artist }}</h4>
      <audio controls autoplay="autoplay">
          <source src="{{$radioSong->song_url}}"/>
      </audio>
  </div>
</div>
@endif
<div class="container">
  <div class="row" id="masonry-container">
    @if ( !$radioStations->count() )
      No Radio Stations
    @else
      @foreach($radioStations as $index => $radioStation)
      <div class="col span-1">
        <div>
          <a href="/radio/play/{{ $radioStation->id }}">{{ $radioStation->name }}</a>
          <div>
          </div>
          <div >

          </div>
        </div>
      </div>
      @endforeach
    @endif
  </div>
</div>
@stop

@section('js')
  <script src=" {{ asset('js/radio.js') }} " type="text/javascript"></script>
@stop
