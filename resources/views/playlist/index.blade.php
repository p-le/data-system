@extends('layouts.default')
@section('content')
<div class="container">
  <div class="video">
    <div class="video-content">
      <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/tU2GtK2hFaQ?list=PLz1iM8YfFbTeqcg6MDY_VlfbMOjTxWnJ6&showinfo=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="vid-item">
      <div class="thumb">
      	<img src="http://img.youtube.com/vi/eg6kNoJmzkY/0.jpg">
      </div>
      <div class="desc">
      	Jessica Hernandez & the Deltas - Dead Brains
      </div>
    </div>
  </div>
</div>
@stop
