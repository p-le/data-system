@extends('layouts.default')
@section('css')
  <link href="{{ asset('css/news.css') }}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
<div class="container">
  <div class="row" id="masonry-container">
    @if ( !$allNews->count() )
      No Lastest News
    @else
    @foreach($allNews as $index => $news)
    <div class="col span-1">
      <div class="news">
        <a href="{{ $news->item_url }}" class="news-url"></a>
        <div class="news-image">
          <img src="{{ $news->img_url }}" class="img-responsive"/>
        </div>
        <div class="news-title">
          {{ $news->title }}
        </div>
        <div class="news-description">
          {{ $news->description }}
        </div>
        <div class="news-info">
          {{ $news->published_date }} {{ $news->source->name }}
        </div>
      </div>
      {{ Form::hidden('email', $news->item_url, array('class' => 'item-url')) }}
    </div>
    @endforeach
    @endif

  </div>
  {!! $allNews->links() !!}
</div>
@stop

@section('js')
<script src="{{ asset('js/news.js') }}" type="text/javascript"></script>
@stop
