<!DOCTYPE html>
<html lang="en">
<head>
  @include('includes.head')
</head>
<body id="app-layout">
    <div id="body-patern-bg"></div>
    <header>
      @include('includes.header-login')
    </header>
    @yield('content')
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
