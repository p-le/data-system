<!DOCTYPE html>
<html>
  <head lang="en">
    @include('includes.head')
    @yield('css')
  </head>
  <body>
    @include('includes.header')
    @yield('content')
    @include('includes.footer')
    @yield('js')
  </body>
</html>
