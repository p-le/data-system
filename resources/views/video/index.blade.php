@extends('layouts.default')
@section('css')
  <link href="{{ asset('css/swiper.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('css/video.css')}}" rel="stylesheet" type="text/css" />
@stop
@section('content')
<nav class="swiper-container">
  <ul class="swiper-wrapper">
  @foreach($allVideoCategories as $index => $videoCategory)
    <li class="swiper-slide"><a href="/video/{{ $videoCategory->name  }}">#{{ $videoCategory->name  }}</a></li>
  @endforeach
  </ul>
</nav>
<div class="video-container-bg">
  <div class="video-container">
    <div class="video-player">
      <iframe src="" width="560" height="315" frameborder="0" allowfullscreen></iframe>
    </div>
  </div>
</div>
<div class="container">
  <div class="row" id="masonry-container">
    @if ( !$allVideos->count() )
      No Lastest News
    @else
    @foreach($allVideos as $index => $video)
    <div class="col span-1">
      <div class="video" data-field-id="{{ $video->video_id }}">
        <div class="video-thumbnails">
          <img src="{{ $video->video_thumbnails_url }}" class="img-responsive"/>
        </div>
        <div class="video-title">
          {{ $video->video_title }}
        </div>
        <div class="video-info">
          {{ $video->published_at }}
        </div>
        {{ Form::hidden('videoId', $video->video_id, array('class' => 'video-id')) }}
      </div>
    </div>
    @endforeach
    @endif
  </div>
  {!! $allVideos->links() !!}
</div>
@stop
@section('js')
<script src="{{ asset('js/vendor/swiper.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/video.js') }}" type="text/javascript"></script>
@stop
