@extends('layouts.default')

@section('css')
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset('css/system.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('content')
<ul class="side-menu">
  <li class="menu-item"><a href="/system/news">News</a></li>
  <li class="menu-item"><a href="/system/video">Video</a></li>
  <li class="menu-item"><a href="/system/videoChannel">Video Channel</a></li>
  <li class="menu-item"><a href="/system/videoCategory">Video Category</a></li>
</ul>
<div class="container">
    <button class="btn btn-success" data-toggle="modal" data-target="#add-modal">Add Category</button>
    {{ Form::open(array(
      'route' => 'view-category',
      'class' => 'form-inline',
      'style' => 'width: 400px; display: inline-block',
      'method' => 'get'))
    }}
      <div class="form-group">
        {{ Form::text('search-name', '', array('class' => 'form-control', 'placeholder' => 'Category Name'))}}
      </div>
      <input type="submit" class="btn btn-info btn-sm" value="Search">
    {{ Form::close() }}
    <div id="add-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          {{ Form::open(array('route' => 'add-category', 'method' => 'post')) }}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Category</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              {{ Form::label('name', 'Category')}}
              {{ Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'New Category Name')) }}
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" class="btn btn-info" value="Add"></input>
            <button class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
    @if(!empty($validator))
      $messages = $validator->messages();
      {{ $messages }}
      @foreach ($messages->all('<li>:message</li>') as $message)
        {{ $message }}
      @endforeach
    @endif

    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Category</th>
          <th>Modify</th>
        </tr>
      </thead>
      <tbody>
        @foreach($categories as $category)
          <tr>
            <td>{{ $category->id }}</td>
            <td>{{ $category->name }}</td>
            <td>
              <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-modal-{{$category->id}}">Delete</button>
              <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#update-modal-{{$category->id}}">Update</button>
            </td>
          </tr>
          <div id="delete-modal-{{$category->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                {{ Form::open(array('route' => array('delete-category', $category->id), 'method' => 'post')) }}
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Delete Parmanently</h4>
                </div>
                <div class="modal-body">
                  <p>Are you sure about this ?</p>
                </div>
                <div class="modal-footer">
                  <input type="submit" value="Delete" class="btn btn-danger" />
                  <button class="btn btn-default" data-dismiss="modal">Cancel</buttom>
                </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
          <div id="update-modal-{{$category->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                {{ Form::open(array('route' => array('update-category', $category->id), 'method' => 'post')) }}
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Modify Category</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    {{ Form::label('name', 'Category')}}
                    {{ Form::text('name', $category->name, array('class' => 'form-control')) }}
                  </div>
                </div>
                <div class="modal-footer">
                  <input type="submit" class="btn btn-info" value="Update"/>
                  <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
        @endforeach
      </tbody>
    </table>
</div>
@stop
@section('js')
  <script src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
@stop
