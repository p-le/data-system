@extends('layouts.default')

@section('css')
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset('css/system.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('content')
<ul class="side-menu">
  <li class="menu-item"><a href="/system/news">News</a></li>
  <li class="menu-item"><a href="/system/video">Video</a></li>
  <li class="menu-item"><a href="/system/videoChannel">Video Channel</a></li>
  <li class="menu-item"><a href="/system/videoCategory">Video Category</a></li>
</ul>
<div class="container">
  {{ Form::open(array(
    'route' => 'view-video',
    'class' => 'form-inline',
    'id' => 'search-form',
    'style' => 'width: 600px; display: inline-block',
    'method' => 'get'))
  }}
    <div class="form-group">
      {{ Form::text('videoTitle', $searchVideoTitle ? $searchVideoTitle : '', array('class' => 'form-control', 'placeholder' => 'Video Title'))}}
    </div>
    <div class="form-group">
      {{ Form::select(
        'channelID', $channels , $selectedChannelID ? $selectedChannelID : null,
        array('class' => 'form-control', 'id' => 'channel-list', 'placeholder' => 'Pick a channel'))
      }}
    </div>
    <div class="form-group">
      {{ Form::select(
        'categoryID', $categories , $selectedCategoryID ? $selectedCategoryID : null,
        array('class' => 'form-control', 'id' => 'category-list', 'placeholder' => 'Pick a category'))
      }}
    </div>
    <input type="submit" class="btn btn-info btn-sm" value="Search">
  {{ Form::close() }}
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>ID</th>
          <th>Video ID</th>
          <th>Video Title</th>
          <th>Channel ID</th>
          <th>Channel Name</th>
        </tr>
      </thead>
      <tbody>
        @if(isset($videos))
          @if($videos->count())
            @foreach($videos as $video)
              <tr>
                <td>{{ $video->id }}</td>
                <td>{{ $video->video_id }}</td>
                <td>{{ $video->video_title }}</td>
                <td>{{ $video->channel_id }}</td>
                <td>{{ $channels[$video->channel_id]}}</td>
              </tr>
            @endforeach
          @else
            <h2 class="text-danger"> Empty </h2>
          @endif
        @else
          <h2 class="text-danger">Please select category and channel</h2>
        @endif
      </tbody>
    </table>
</div>
@stop
@section('js')
  <script src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
  <script>
    $(function() {
      $('#category-list').change(function() {
        $('#search-form').submit();
      })
      $('#channel-list').change(function() {
        $('#search-form').submit();
      })
    });
  </script>
@stop
