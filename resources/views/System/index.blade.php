@extends('layouts.default')

@section('css')
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/system.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('content')
@stop

<div class="container">
  <div class="wrapper">
    <div class="row">
      <div class="col-sm-6">
        <div class="menu-item">
          <a href="/system/news">News</a>
        </div>
      </div>
      <div class="col-sm-6 ">
        <div class="menu-item">
          <a href="/system/video">Video</a>
        </div>
      </div>
    </div>
    <div class="row" style="margin-top: 10px;">
      <div class="col-sm-6">
        <div class="menu-item">
          <a href="/system/videoChannel">Video Channel</a>
        </div>
      </div>
      <div class="col-sm-6">
        <div class=" menu-item">
          <a href="/system/videoCategory">Video Category</a>
        </div>
      </div>
    </div>
    <div class="row" style="margin-top: 10px;">
      <div class="col-sm-6">
        <div class="menu-item">
          <a href="/system/radio">Radio Station</a>
        </div>
      </div>
    </div>
  </div>
</div>
@section('js')
@stop
