@extends('layouts.default')

@section('css')
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset('css/system.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('content')
<ul class="side-menu">
  <li class="menu-item"><a href="/system/news">News</a></li>
  <li class="menu-item"><a href="/system/video">Video</a></li>
  <li class="menu-item"><a href="/system/videoChannel">Video Channel</a></li>
  <li class="menu-item"><a href="/system/videoCategory">Video Category</a></li>
</ul>
<div class="container">
  <button class="btn btn-success" data-toggle="modal" data-target="#add-modal">Add Channel</button>
  {{ Form::open(array(
    'route' => 'view-channel',
    'class' => 'form-inline',
    'style' => 'width: 400px; display: inline-block',
    'method' => 'get'))
  }}
    <div class="form-group">
      {{ Form::text('search-name', '', array('class' => 'form-control', 'placeholder' => 'Channel Name'))}}
    </div>
    <div class="form-group">
      {{ Form::select(
        'search-category', $categories , null,
        array('class' => 'form-control', 'placeholder' => 'Pick a category'))
      }}
    </div>
    <input type="submit" class="btn btn-info btn-sm" value="Search">
  {{ Form::close() }}
  <div id="add-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        {{ Form::open(array('route' => 'add-channel', 'method' => 'post')) }}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Channel</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            {{ Form::label('name', 'Channel Name')}}
            {{ Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'New Channel Name')) }}
          </div>
          <div class="form-group">
            {{ Form::label('channelID', 'Channel ID')}}
            {{ Form::text('channelID', '', array('class' => 'form-control', 'placeholder' => 'New Channel ID')) }}
            <span class="text-info">※ Use youtube data api to get ChannelID from Channel Name</span>
            <span class="text-info">https://www.googleapis.com/youtube/v3/channels?key=AIzaSyD4wIKoxg645dCjM-PxuOJ6nqZpkJ-3d5w&forUsername={ChannelName}&part=id</span>
          </div>
          <div class="form-group">
            {{ Form::label('channelCategory', 'Category')}}
            {{ Form::select(
              'channelCategory', $categories , null,
              array('class' => 'form-control', 'placeholder' => 'Pick a category'))
            }}
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-info" value="Add"></input>
          <button class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
  <table class="table table-bordered table-hover table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Channel Name</th>
        <th>Channel ID</th>
        <th>Channel Category</th>
        <th>Modify</th>
      </tr>
    </thead>
    <tbody>
      @foreach($channels as $channel)
        <tr>
          <td>{{ $channel->id }}</td>
          <td>{{ $channel->name }}</td>
          <td>{{ $channel->channel_id }}</td>
          <td>{{ $channel->category_id }}</td>
          <td>
            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-modal-{{$channel->id}}">Delete</button>
            <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#update-modal-{{$channel->id}}">Update</button>
          </td>
          <div id="delete-modal-{{$channel->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                {{ Form::open(array('route' => array('delete-channel', $channel->id), 'method' => 'post')) }}
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Delete Parmanently</h4>
                </div>
                <div class="modal-body">
                  <p>Are you sure about this ?</p>
                </div>
                <div class="modal-footer">
                  <input type="submit" value="Delete" class="btn btn-danger" />
                  <button class="btn btn-default" data-dismiss="modal">Cancel</buttom>
                </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
          <div id="update-modal-{{$channel->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                {{ Form::open(array('route' => array('update-channel', $channel->id), 'method' => 'post')) }}
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Modify Channel</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    {{ Form::label('name', 'Channel Name')}}
                    {{ Form::text('name', $channel->name, array('class' => 'form-control')) }}
                  </div>
                  <div class="form-group">
                    {{ Form::label('channelID', 'Channel Name')}}
                    {{ Form::text('channelID', $channel->channel_id, array('class' => 'form-control')) }}
                  </div>
                  <div class="form-group">
                    {{ Form::label('channelCategory', 'Category')}}
                    {{ Form::select(
                      'channelCategory', $categories , $channel->category_id,
                      array('class' => 'form-control'))
                    }}
                  </div>
                </div>
                <div class="modal-footer">
                  <input type="submit" class="btn btn-info" value="Update"/>
                  <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
@stop
@section('js')
  <script src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
@stop
