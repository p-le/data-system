<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-login">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <a class="navbar-brand" href="{{ url('/') }}">Tin Tức Việt Nam</a>

  </div>
  <div class="collapse navbar-collapse" id="nav-login">
    <ul class="nav navbar-nav navbar-right" style="margin-right: 10px">
        @if (Auth::guest())
            <li><a href="{{ url('/') }}">Đăng Nhập</a></li>
            <li><a href="{{ url('/register') }}">Đăng Kí</a></li>
        @else
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                </ul>
            </li>
        @endif
  </div>
</nav>
