<footer>
  <p>
    @Copyright: Phu Le
  </p>
</footer>

<script src="{{ asset('js/vendor/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/vendor/masonry.pkgd.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/vendor/imagesloaded.pkgd.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/vendor/jquery.infinitescroll.js') }}" type="text/javascript"></script>

<script src="{{ asset('js/common.js') }}" type="text/javascript"></script>

<script type="text/javascript">
  $.ajaxSetup({
     headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
