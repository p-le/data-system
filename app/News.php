<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model {
  protected $table = 'items';

  public function source() {
    return $this->belongsTo('App\Source', 'source_id');
  }
}
