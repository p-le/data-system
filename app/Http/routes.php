<?php

Route::post('/trackingClick', 'Tracking\ClickController@process');
Route::post('/trackingImpression', 'Tracking\ImpressiobController@process');

Route::group(['middleware' => 'web'], function () {
    Route::get('/', function() {
      return view('homepage');
    });
    Route::get('/news', 'NewsController@index');
    Route::get('/home', 'HomeController@index');

    Route::get('/video', 'VideoController@index');
		Route::get('/video/{category}', 'VideoController@category');

    Route::get('/radio', 'RadioController@index');
    Route::get('/radio/play/{id}', 'RadioController@play');
    Route::get('/radio/next/{id}', 'RadioController@next');

    Route::get('/system/', 'SystemController@index');

    Route::get('/system/news', 'SystemController@news');

    Route::get('/system/video', 'SystemController@video')->name('view-video');

    Route::get('/system/videoChannel', 'SystemController@videoChannel')->name('view-channel');
    Route::post('/system/videoChannel/add', 'SystemController@addVideoChannel')->name('add-channel');
    Route::post('/system/videoChannel/delete/{id}', 'SystemController@deleteVideoChannel')->name('delete-channel');
    Route::post('/system/videoChannel/update/{id}', 'SystemController@updateVideoChannel')->name('update-channel');

    Route::get('/system/videoCategory/', 'SystemController@videoCategory')->name('view-category');
    Route::post('/system/videoCategory/add', 'SystemController@addVideoCategory')->name('add-category');
    Route::post('/system/videoCategory/delete/{id}', 'SystemController@deleteVideoCategory')->name('delete-category');
    Route::post('/system/videoCategory/update/{id}', 'SystemController@updateVideoCategory')->name('update-category');
});
