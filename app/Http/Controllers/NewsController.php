<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/* Import Model*/
use App\News;
use App\Source;

use Agent;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsController extends Controller {
  protected $itemNum = 10;

  public function index() {
    if(Agent::isPhone()) {
      $this->itemNum = 7;
    }
    $allNews = News::orderBy('published_date', 'desc')->paginate($this->itemNum);
    $allSources = Source::all();
    return view('news.index')->with([
      'allNews' => $allNews,
      'allSources' => $allSources,
    ]);
  }

}
