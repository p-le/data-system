<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\RadioStation;
use App\Song;

use App\Http\Requests;

class RadioController extends Controller {
  public function index() {
    $radioStations = RadioStation::all();
    return view('radio.index')->with([
      'radioStations' => $radioStations
    ]);
  }

  public function play($id) {
    $radio = RadioStation::find($id);
    $radioSong = Song::where('station_id', $id)->orderByRaw("RAND()")->first();
    $radioStations = RadioStation::all();
    return view('radio.index')->with([
      'radioSong' => $radioSong,
      'radio' => $radio,
      'radioStations' => $radioStations
    ]);
  }
  public function next($id) {
    $radioSong = Song::where('station_id', $id)->orderByRaw("RAND()")->first();
    return response()->json($radioSong->toArray());
  }
}
