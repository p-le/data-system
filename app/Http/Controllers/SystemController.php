<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Video;
use App\VideoCategory;
use App\News;
use App\VideoChannel;

use App\Http\Requests;

use Validator;
use Redirect;

class SystemController extends Controller {

  public function index() {
    return view('System.index');
  }

  public function news() {

  }
  public function video(Request $request) {
    $categories = VideoCategory::lists('name', 'id')->toArray();
    $categoryID = $request->input('categoryID');
    $channelID = $request->input('channelID');
    $videoTitle = $request->input('Video Title');
    if($categoryID) {
      $searchChannels = VideoChannel::where('category_id', $categoryID);
      $channels = $searchChannels->lists('name', 'id')->toArray();
    } else {
      $channels = VideoChannel::lists('name', 'id')->toArray();
    }
    $query = Video::select();
    $resultReady = false;
    if($categoryID) {
      $query->whereIn('channel_id', $searchChannels->lists('id')->toArray());
      $resultReady = true;
    }
    if($channelID) {
      $query->where('channel_id', $channelID);
      $resultReady = true;
    }
    if($channelID) {
      $query->where('channel_id', $channelID);
      $resultReady = true;
    }
    if($videoTitle) {
      $query->where('video_title', 'LIKE', '%'.$channelID.'%');
      $resultReady = true;
    }
    $videos = $query->get();
    return view('System.video')->with([
      'videos' => $resultReady ? $videos : null,
      'categories' => $categories,
      'channels' => $channels,
      'selectedCategoryID' => $categoryID,
      'selectedChannelID' => $channelID,
      'searchVideoTitle' => $videoTitle
    ]);
  }

  /* =================== VIDEO CHANNEL ====================*/
  public function videoChannel(Request $request) {
    $searchName = $request->input('search-name');
    $searchCategory = $request->input('search-category');
    $query = VideoChannel::select();
    if($searchName || $searchCategory) {
      !empty($searchName) ? $query->where('name', $searchName) : null;
      !empty($searchCategory) ? $query->where('category_id', $searchCategory) : null;
      $channels = $query->get();
    } else {
      $channels = VideoChannel::all();
    }

    $categories = VideoCategory::lists('name', 'id')->toArray();
    return view('System.videoChannel')->with([
      'channels' => $channels,
      'categories' => $categories
    ]);
  }
  public function addVideoChannel(Request $request) {
    $validator = Validator::make($request->all(), [
      'name' => 'required|max:255|unique:video_channels,name',
      'channelID' => 'required|unique:video_channels,channel_id',
      'channelCategory' => 'required'
    ]);
    if($validator->fails()) {
      return Redirect::to('system/videoChannel')->withErrors($validator);
    } else {
      $channel = new VideoChannel;
      $channel->name = $request->input('name');
      $channel->channel_id = $request->input('channelID');
      $channel->category_id = $request->input('channelCategory');
      $channel->save();
      return Redirect::to('system/videoChannel');
    }
  }
  public function updateVideoChannel(Request $request, $id) {
    $channel = VideoChannel::findOrFail($id);
    $channel->name = $request->input('name');
    $channel->channel_id = $request->input('channelID');
    $channel->category_id = $request->input('channelCategory');
    $channel->save();
    return Redirect::to('system/videoChannel');
  }
  public function deleteVideoChannel($id) {
    VideoChannel::findOrFail($id)->delete();
    Video::where('channel_id', $id)->delete();
    return Redirect::to('system/videoChannel');
  }
  /* =================== VIDEO CHANNEL ====================*/
  /* =================== VIDEO CATEGORY ====================*/
  public function videoCategory(Request $request) {
    if($request->input('search-name')) {
      $name = $request->input('search-name');
      $categories = VideoCategory::where('name', $name)->get();
    } else {
      $categories = VideoCategory::all();
    }
    return view('System.videoCategory')->with([
      'categories' => $categories
    ]);
  }
  public function addVideoCategory(Request $request) {
    $validator = Validator::make($request->all(), [
      'name' => 'required|max:255|unique:video_categories,name'
    ]);
    if($validator->fails()) {
      return Redirect::to('system/videoCategory')->withErrors($validator);
    } else {
      $category = new VideoCategory;
      $category->name = $request->input('name');
      $category->save();
      return Redirect::to('system/videoCategory');
    }
  }
  public function updateVideoCategory(Request $request, $id) {
    $category = VideoCategory::find($id);
    $category->name = $request->input('name');
    $category->save();
    $categories = VideoCategory::all();
    return view('System.videoCategory')->with([
      'categories' => $categories
    ]);
  }
  public function deleteVideoCategory($id) {
    $category = VideoCategory::findOrFail($id);
    $channels = VideoChannel::where('category_id', $id)->get();
    if($channels) {
      foreach($channels as $channel) {
        $videos = Video::where('channel_id', $channel->id)->get();
        if($videos) {
          Video::where('channel_id', $channel->id)->delete();
        }
        VideoChannel::where('category_id', $id)->delete();
      }
    }
    $category->delete();
    return Redirect::to('system/videoCategory');
  }
  /* =================== VIDEO CATEGORY ====================*/
}
