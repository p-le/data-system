<?php

namespace App\Http\Controllers\Tracking;

use Input;
use Request;
use App\News;
use App\Models\SummaryHourly;

use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClickController extends Controller {
  public function process() {
    if(Request::ajax()) {
      /* ----- CSRF PREVENT ----- */
      
      /* ----- ----- ----- ----- */
      $news = News::where('item_url', '=', Request::input('url'))->firstOrFail();
      $now = Carbon::now();

      $instance = SummaryHourly::firstOrNew(array(
        'item_id' => $news->id,
        'hour' => $now->hour,
        'date' => $now->toDateString()
      ));

      if($instance->exists) {
        $instance->total_click++;
      } else {
        $instance->total_click = 1;
      }
      $instance->total_impression = 0;

      $instance->save();
    } else {
      /* TODO */
    }
  }
}
