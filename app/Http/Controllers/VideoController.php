<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Video;
use App\VideoCategory;
use App\VideoChannel;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VideoController extends Controller {
  protected $pageLimit = 15;

  public function index() {
    $allVideos = Video::orderBy('published_at', 'desc')->paginate($this->pageLimit);
    $allVideoCategories = VideoCategory::all();
    return view("video.index")->with([
      'allVideos' => $allVideos,
      'allVideoCategories' => $allVideoCategories
    ]);
  }

	public function category($category) {
		$categoryRes = VideoCategory::where('name', $category)->select('id')->take(1)->get();
		$videoChannelRes = VideoChannel::where('category_id', $categoryRes[0]['id'])->select('id')->get();
		$channelIds = array();
		foreach($videoChannelRes as $res) {
			$channelIds[] = $res['id'];
		}
		$allVideos = Video::whereIn('channel_id', $channelIds)->orderBy('published_at', 'desc')->paginate($this->pageLimit);
    $allVideoCategories = VideoCategory::all();
		return view("video.index")->with([
			'allVideos' => $allVideos,
			'allVideoCategories' => $allVideoCategories
		]);
	}
}
