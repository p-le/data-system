<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SummaryMonthly extends Model {
  protected $table = 'track_summary_monthly';
}
