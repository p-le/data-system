<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SummaryDaily extends Model {
  protected $table = 'track_summary_daily';
}
