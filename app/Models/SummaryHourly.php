<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SummaryHourly extends Model {
  protected $table = 'track_summary_hourly';
  protected $fillable = ['item_id', 'hour', 'date', 'total_click', 'total_impression'];
  
  public $timestamps = false;

  public function item() {
    return $this->belongTo('App\News');
  }
}
