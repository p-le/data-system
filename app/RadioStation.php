<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RadioStation extends Model {
  protected $table = "radio_stations";
  public $timestamps = false;
}
