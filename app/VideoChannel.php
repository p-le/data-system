<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoChannel extends Model {
  protected $table = "video_channels";
  public $timestamps = false;
}
