var elixir = require('laravel-elixir');

var dirs = {
  'bootstrap' : 'vendor/bower_components/bootstrap/',
  'jquery' : 'vendor/bower_components/jquery/',
  'fontawesome' : 'vendor/bower_components/font-awesome/',
  'masonry' : 'vendor/bower_components/masonry/',
  'imagesloaded' : 'vendor/bower_components/imagesloaded/',
  'swiper' : 'vendor/bower_components/Swiper/'
}


elixir(function(mix) {
    /* CSS MOVE */
    mix.copy(dirs.bootstrap + 'dist/css/bootstrap.min.css', 'public/css')
      .copy(dirs.bootstrap + 'dist/css/bootstrap-theme.min.css', 'public/css')
      .copy(dirs.swiper + 'dist/css/swiper.min.css', 'public/css');
    /* JS MOVE */
    mix.copy(dirs.jquery + 'dist/jquery.min.js', 'public/js/vendor')
      .copy(dirs.bootstrap + 'dist/js/bootstrap.min.js', 'public/js/vendor')
      .copy(dirs.masonry + 'dist/masonry.pkgd.min.js', 'public/js/vendor')
      .copy(dirs.imagesloaded + 'imagesloaded.pkgd.min.js', 'public/js/vendor')
      .copy(dirs.swiper + 'dist/js/swiper.jquery.min.js', 'public/js/vendor');
    /* FONTS MOVE */
    mix.copy(dirs.bootstrap + 'dist/fonts', 'public/fonts');
});


// elixir(function(mix) {
//     mix.sass('bootstrap.scss')
//       .styles([
//           paths.fontawesome + 'css/font-awesome.css'
//       ], 'public/css/fontawesome.css', './')
//       .copy(paths.bootstrap + 'fonts/bootstrap', 'public/fonts')
//       .copy(paths.fontawesome + 'fonts', 'public/fonts')
//       .scripts([
//         paths.jquery + "dist/jquery.js",
//         paths.bootstrap + "javascripts/bootstrap.js",
//         paths.masonry + "dist/masonry.pkgd.js",
//         paths.imagesloaded + "imagesloaded.pkgd.js"
//       ], 'public/js/app.js', './');
// });
