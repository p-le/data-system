<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackSummaryMonthlyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

      Schema::create('track_summary_monthly', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('item_id')->length(20)->unsigned();
        $table->tinyInteger('month');
        $table->date('date');
        $table->integer('total_click');
        $table->integer('total_impression');

      });

      Schema::table('track_summary_monthly', function(Blueprint $table) {
        $table->foreign('item_id')->references('id')->on('items');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('track_summary_monthly');
    }
}
