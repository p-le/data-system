<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoChannelsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('video_channels', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name', 255);
      $table->string('channel_id', 32);
      $table->integer('category_id');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('video_channels');
  }
}
