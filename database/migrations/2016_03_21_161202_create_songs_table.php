<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('songs', function (Blueprint $table) {
        $table->bigInteger('code')->unique();
        $table->string('name', 256);
        $table->string('artist', 256);
        $table->mediumText('song_url');
        $table->mediumText('cover_url')->nullable();
        $table->mediumText('lyrics')->nullable();
        $table->integer('station_id');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('songs');
  }
}
