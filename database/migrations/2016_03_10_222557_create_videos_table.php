<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
						$table->string('video_id', 45)->unique();
						$table->mediumText('video_title');
            $table->mediumText('video_thumbnails_url');
            $table->time('video_duration')->nullable();
						$table->datetime('published_at');
						$table->integer('channel_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }
}
