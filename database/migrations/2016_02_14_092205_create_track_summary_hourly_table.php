<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackSummaryHourlyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      Schema::create('track_summary_hourly', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('item_id')->length(20)->unsigned();
        $table->integer('hour');
        $table->date('date');
        $table->integer('total_click');
        $table->integer('total_impression');
      });

      Schema::table('track_summary_hourly', function(Blueprint $table) {
        $table->foreign('item_id')->references('id')->on('items');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('track_summary_hourly');
    }
}
