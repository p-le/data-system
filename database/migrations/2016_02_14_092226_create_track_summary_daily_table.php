<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackSummaryDailyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      Schema::create('track_summary_daily', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('item_id')->length(20)->unsigned();
        $table->date('date');
        $table->bigInteger('total_click');
        $table->bigInteger('total_impression');
      });

      Schema::table('track_summary_daily', function(Blueprint $table) {
        $table->foreign('item_id')->references('id')->on('items');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('track_summary_daily');
    }
}
