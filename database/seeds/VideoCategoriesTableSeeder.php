<?php

use Illuminate\Database\Seeder;

class VideoCategoriesTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    DB::table('video_categories')->truncate();
    DB::table('video_categories')->insert([
      [
        'name' => 'comedy'
      ],
      [
        'name' => 'education'
      ],
      [
        'name' => 'entertainment'
      ],
      [
        'name' => 'film'
      ],
      [
        'name' => 'games'
      ],
      [
        'name' => 'howto'
      ],
      [
        'name' => 'music'
      ],
      [
        'name' => 'news'
      ],
      [
        'name' => 'animals'
      ],
      [
        'name' => 'tech'
      ]
    ]);
  }


}
