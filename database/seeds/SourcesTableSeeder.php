<?php

use Illuminate\Database\Seeder;

class SourcesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      DB::table('sources')->truncate();

      if(Storage::exists('sources.txt')) {
        print "Sources.txt Found! ".PHP_EOL;
        $content = Storage::get('sources.txt');
        $sources = explode(PHP_EOL, $content);

        /* Loop through sources array */
        foreach ($sources as $source) {
          if(strlen($source) > 0) {
            DB::table('sources')->insert([
              'name' => trim($source)
            ]);
          }
        }
      } else {
        print "sources.txt does not exist. Please create it!".PHP_EOL;
      }
    }
}
