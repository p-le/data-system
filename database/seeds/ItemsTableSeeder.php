<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      /* Clear Database */
      DB::table('items')->truncate();

      DB::table('items')->insert([
        [
          'title' => 'Chị em nhà Jenner trình làng thương hiệu thời trang bình dân',
          'description' => 'TTO - Hai chị em Kendall và Kylie Jenner vừa giới thiệu bộ sưu tập đầu tiên trong thương hiệu thời trang của mình.',
          'img_url' => 'http://static.new.tuoitre.vn/tto/i/s146/2016/01/31/kendall-jenner-kylie-1454219921.jpg',
          'item_url' => 'http://tuoitre.vn/tin/van-hoa-giai-tri/20160131/chi-em-nha-jenner-trinh-lang-thuong-hieu-thoi-trang-binh-dan/1047112.html',
          'published_date' => new Datetime,
          'md5sum' => md5('http://tuoitre.vn/tin/van-hoa-giai-tri/20160131/chi-em-nha-jenner-trinh-lang-thuong-hieu-thoi-trang-binh-dan/1047112.html'),
          'source_id' => 1
        ],
        [
          'title' => '​Tiến Truyển đăng quang Project Runway Việt Nam mùa 3',
          'description' => 'TTO - Chung kết "Project Runway Vietnam" mùa 3 kết thúc với chiến thắng thuộc về NTK Tiến Truyển, NTK Trần Hùng là Á quân 1, Á quân 2 thuộc về NTK Giang Tú.',
          'img_url' => 'http://static.new.tuoitre.vn/tto/i/s146/2016/01/31/project-runway-vietnam-jpg-1454216016.jpg',
          'item_url' => 'http://tuoitre.vn/tin/van-hoa-giai-tri/20160131/tien-truyen-dang-quang-project-runway-viet-nam-mua-3/1047421.html',
          'published_date' => new Datetime,
          'md5sum' => md5('http://tuoitre.vn/tin/van-hoa-giai-tri/20160131/chi-em-nha-jenner-trinh-lang-thuong-hieu-thoi-trang-binh-dan/10412.html'),
          'source_id' => 1
        ],
        [
          'title' => 'Ngắm thiết kế "Châu Á tương lai" tại chung kết ​Project Runway',
          'description' => 'TTO - Ngắm các mẫu thiết kế "Châu Á tương lai" tại chung kết ​Project Runway Việt Nam của ba nhà thiết kế trẻ Tiến Truyển (quấn quân), Trần Hùng (Á quân 1) và Giang Tú (Á quân 2)',
          'img_url' => 'http://static.new.tuoitre.vn/tto/i/s146/2016/01/31/project-runway-vietnam-jpg-1454217421.jpg',
          'item_url' => 'http://tuoitre.vn/tin/van-hoa-giai-tri/20160131/ngam-thiet-ke-chau-a-tuong-lai-tai-chung-ket-project-runway/1047427.html',
          'md5sum' => md5('http://tuoitre.vn/tin/van-hoa-giai-tri/20160131/chi-em-nha-jenner-trinh-lang-thuong-hieu-thoi-trang-h-dan/1047112.html'),
          'published_date' => new Datetime,
          'source_id' => 1
        ],
        [
          'title' => ' Nhiều tuyến đường TP.HCM ù ứ nghiêm trọng ',
          'description' => 'Chiều 2.2, nhiều nút giao thông ở trung tâm TP.HCM xảy ra tình trạng ùn ứ nghiêm trọng. ',
          'img_url' => 'http://static.thanhnien.com.vn/Uploaded/vuphuong/2016_02_02/a2_IRRQ.jpg',
          'item_url' => 'http://thanhnien.vn/chinh-tri-xa-hoi/nhieu-tuyen-duong-tphcm-u-u-nghiem-trong-664890.html',
          'published_date' => new Datetime,
          'md5sum' => md5('http://tuoitre.vn/tin/van-hoa-giai-tri/20160131/chi-em-nha-jeh-lang-thuong-hieu-thoi-trang-binh-dan/1047112.html'),
          'source_id' => 1
        ],
        [
          'title' => 'Các cột rút tiền ATM đồng loạt báo lỗi',
          'description' => ' Mặc dù các ngân hàng đều khẳng định không thiếu tiền trong dịp Tết, nhưng những ngày gần đây, nhiều cột ATM ở Hà Nội đều không rút được tiền.  ',
          'img_url' => 'http://static.thanhnien.com.vn/Uploaded/thuhang/2016_02_02/atmkhongrutduoctien_KYXM.jpg',
          'item_url' => 'http://thanhnien.vn/kinh-te/cac-cot-rut-tien-atm-dong-loat-bao-loi-664867.html',
          'md5sum' => md5('http://tuoi/van-hoa-giai-tri/20160131/chi-em-nha-jenner-trinh-lang-thuong-hieu-thoi-trang-binh-dan/1047112.html'),
          'published_date' => new Datetime,
          'source_id' => 1
        ],
        [
          'title' => '[VIDEO] Cháy ở khách sạn 8 tầng, nhiều người hoảng loạn ',
          'description' => 'Hàng chục người bỏ chạy tán loạn khi ngọn lửa bùng phát ở tầng thượng của một khách sạn 8 tầng nằm trên đường đường Lương Hữu Khánh (thuộc P.Phạm Ngũ Lão, Q.1, TP.HCM). ',
          'img_url' => 'http://static.thanhnien.com.vn/Uploaded/tranduy/2016_02_02/chaykhachsan8tang_BVEI.jpg',
          'item_url' => 'http://thanhnien.vn/chinh-tri-xa-hoi/video-chay-o-khach-san-8-tang-nhieu-nguoi-hoang-loan-664884.html',
          'md5sum' => md5('http://tuoitre.vn/tin/van-hoa-giai-tri/1/chi-em-nha-jenner-trinh-lang-thuong-hieu-thoi-trang-binh-dan/1047112.html'),
          'published_date' => new Datetime,
          'source_id' => 1
        ],
        [
          'title' => 'Công bố đường dây nóng để người dân tố xe nhồi nhét xe khách ',
          'description' => 'Ủy ban An toàn giao thông quốc gia vừa công bố 12 số điện thoại đường dây nóng để người dân phản ánh về tình hình an toàn giao thông, hoạt động kinh doanh vận tải dịp Tết Bính Thân, Lễ hội xuân 2016. ',
          'img_url' => 'http://static.thanhnien.com.vn/Uploaded/maithu/2016_02_02/xebatkhachdocduong_HUMB.jpg',
          'item_url' => 'http://thanhnien.vn/chinh-tri-xa-hoi/cong-bo-duong-day-nong-de-nguoi-dan-to-xe-nhoi-nhet-xe-khach-664855.html',
          'md5sum' => md5('http://tuoitre.vn/tin/van-hoa-giai-tri/20160131/cha-jenner-trinh-lang-thuong-hieu-thoi-trang-binh-dan/1047112.html'),
          'published_date' => new Datetime,
          'source_id' => 1
        ]
      ]);
    }
}
