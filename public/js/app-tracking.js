$('.news-panel').one('click', function(e) {
  $hiddenInput = $(this).find('.item-url');
  $ajaxData  = $hiddenInput.val();

  if(e.which === 1 ) {
    $.ajax({
      type: 'POST',
      url: '/trackingClick',
      data: {
        'url' : $ajaxData
      }
    });
  }
});

$('.news-panel').one('mouseenter', function(e) {
  $hiddenInput = $(this).find('.item-url');
  $ajaxData  = $hiddenInput.val();

  if(e.which === 1 ) {
    $.ajax({
      type: 'POST',
      url: '/trackingImpression',
      data: {
        'url' : $ajaxData
      }
    });
  }
});
