// (function($) {
//   $('#video-container .more button').click(function() {
//     window.location.href= '/videos';
//   });
//   $('#news-container .more button').click(function() {
//     window.location.href= '/news';
//   })
// }) (jQuery);

(function($) {
  $('#menu-button').on('touchstart click', function(e) {
    e.preventDefault();

    var $body = $('body'),
      $container = $('.container'),
      $menu = $('#menu'),
      transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';

    if ($body.hasClass('opening-menu')) {
      $body.removeClass('opening-menu');
      $menu.removeClass('opening');
    } else {
      $body.addClass('opening-menu');
    }

    $container.on(transitionEnd, function() {
      if ($body.hasClass('opening-menu')) {
        $menu.addClass('opening');
      }
      $container.off(transitionEnd);
    });
  });

  var mySwiper = new Swiper('.swiper-container', {
    loop: true,
    pagination: '.swiper-pagination'
  });

  /* Video Nav Active Check*/
  $('#hot-video-nav li').on('touchstart click', function(e) {
    if (!$(this).hasClass('current')) {
      $current = $('#hot-video-nav li.current');
      $current.removeClass('current');
      $(this).addClass('current');
      $last = $current;
      switch ($(this).attr('id')) {
        case 'movie-menu':
          $('#movie').removeClass('hide');
          break;
        case 'music-menu':
          $('#music').removeClass('hide');
          break;
        case 'psy-menu':
          $('#psychology').removeClass('hide');
          break;
        case 'funny-menu':
          $('#funny').removeClass('hide');
          break;
      };
      switch ($last.attr('id')) {
        case 'movie-menu':
          $('#movie').addClass('hide');
          break;
        case 'music-menu':
          $('#music').addClass('hide');
          break;
        case 'psy-menu':
          $('#psychology').addClass('hide');
          break;
        case 'funny-menu':
          $('#funny').addClass('hide');
          break;
      }
    }
  });
  /* ===================== */

})(jQuery);
