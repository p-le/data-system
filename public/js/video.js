$(function(){
  $('.video-container-bg').hide();
  $('ul.pagination').css('opacity', 0);

  var $masonryContainer = $('#masonry-container');

  $masonryContainer.imagesLoaded( function() {
    $masonryContainer.masonry({
      itemSelector: '.span-1',
      columnWidth: '.span-1',
      isAnimated: true
    });
  });
  $masonryContainer.infinitescroll({
    navSelector  : "ul.pagination",
    nextSelector : "ul.pagination a:first",
    itemSelector : ".span-1",

    loading: {
      finishedMsg: 'No more pages to load.'
      }
    },
    function( newElements ) {
      var $newElems = $( newElements ).css({ opacity: 0 });
      $newElems.imagesLoaded(function(){
        $newElems.animate({ opacity: 1 });
        $masonryContainer.masonry( 'appended', $newElems, true );
      });
    }
  );

  var swiper = new Swiper('.swiper-container', {
      pagination: '.swiper-pagination',
      slidesPerView: 5,
      spaceBetween: 3,
      freeMode: true,
      loop: true
  });

  $('#masonry-container').on('click', '.video', function(e) {
    $('.video-container-bg').show();
    $('.video-player iframe').attr('src', 'https://www.youtube.com/embed/'
      + $(this).find('.video-id').val() + '?rel=0&autoplay=1');
    e.preventDefault();
    $('.video-container-bg').css('top', $('.swiper-container').height());
    $('.container').css('margin-top', $('.video-container').height());
  });
});
