var widthOfList = function() {
  var itemsWidth = 0;
  $('.navbar-scroll .nav-tabs li').each(function() {
    var itemWidth = $(this).outerWidth();
    itemsWidth += itemWidth;
  });
  return itemsWidth;
};

var getLeftPosi = function() {
  return $('.navbar-scroll .nav-tabs').position().left;
};

var reAdjust = function() {
  if (($('.navbar-scroll').outerWidth() - getLeftPosi()) < widthOfList()) {
    $('.scroller-right').show();
  } else {
    $('.scroller-right').hide();
  }

  if (getLeftPosi() < 0) {
    $('.scroller-left').show();
  }
}

$('.scroller-right').click(function() {
  $('.scroller-left').fadeIn('slow');
  $('.scroller-right').fadeOut('slow');
  $('.navbar-scroll .nav-tabs').animate({
      left: "-=500px"
    },
    'slow',
    function() {
      reAdjust();
    }
  );
});

$('.scroller-left').click(function() {
  $('.scroller-right').fadeIn('slow');
  $('.scroller-left').fadeOut('slow');
  console.log(getLeftPosi());
  if(getLeftPosi() < 0 && 0-getLeftPosi() > 500) {
    $('.navbar-scroll .nav-tabs').animate({
        left: "+=500px"
      },
      'slow',
      function() {
        reAdjust();
      }
    );
  } else if(0 < 0-getLeftPosi() < 500) {
    $('.navbar-scroll .nav-tabs').animate({
        left: "3px"
      },
      'slow',
      function() {
        reAdjust();
      }
    );
  } else {
    reAdjust();
  }
});

$(window).on('resize', function(e) {
  reAdjust();
});

reAdjust();
