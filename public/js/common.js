(function($) {
  $('#menu-button').on('touchstart click', function(e) {
    e.preventDefault();

    var $body = $('body'),
      $container = $('.container'),
      $menu = $('#menu'),
      transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';

    if ($body.hasClass('opening-menu')) {
      $body.removeClass('opening-menu');
      $menu.removeClass('opening');
    } else {
      $body.addClass('opening-menu');
    }

    $container.on(transitionEnd, function() {
      if ($body.hasClass('opening-menu')) {
        $menu.addClass('opening');
      }
      $container.off(transitionEnd);
    });
  });
})(jQuery);
