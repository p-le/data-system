$(function(){
  $('ul.pagination').css('opacity', 0);

  var $masonryContainer = $('#masonry-container');

  $masonryContainer.imagesLoaded( function() {
    $masonryContainer.masonry({
      itemSelector: '.span-1',
      columnWidth: '.span-1',
      isAnimated: true
    });
  });
  $masonryContainer.infinitescroll({
    navSelector  : "ul.pagination",
    nextSelector : "ul.pagination a:first",
    itemSelector : ".span-1",

    loading: {
      finishedMsg: 'No more pages to load.'
      }
    },
    function( newElements ) {
      var $newElems = $( newElements ).css({ opacity: 0 });
      $newElems.imagesLoaded(function(){
        $newElems.animate({ opacity: 1 });
        $masonryContainer.masonry( 'appended', $newElems, true );
      });
    }
  );
});
/* ====== SCROLL LOADING ====== */
$('#masonry-container').on('click', '.news', function() {
  $url = $(this).find('a.news-url').attr('href');
	window.open($url, '_blank');
});
