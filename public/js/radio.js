$(function() {
  var audio = $('audio')
  audio.on('ended', function() {
    $radioID = 1;
    $.ajax({
     url : '/radio/next/' + $radioID,
     type : 'GET',
     success : function(data) {
       $('audio source').attr('src', data['song_url']);
       audio.trigger('pause');
       audio.trigger('load');
       audio.trigger('play');
       $('.cover img').attr('src', data['cover_url']);
       $('.song-info').text(data['name'] + ' - ' + data['artist']);
     }
    })
  });
});
